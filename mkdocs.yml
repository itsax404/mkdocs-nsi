site_name: Cours de mathématiques
site_url: https://zeepeen.gitlab.io/mkdocs-nsi/
repo_url: https://gitlab.com/zeepeen/mkdocs-nsi
edit_uri: tree/master/docs/
site_description: Site contenant des cours de mathématiques !
docs_dir: docs
copyright: |
    Copyleft ® 2021 <a href="https://www.gitlab.com/zeepeen/mkdocs-nsi/" target="_blank" rel="noopener">Quentin DAVIN</a>

nav:
    - Accueil: index.md
    - Somme de variables aléatoires: variables_aléatoires.md
    - Aide-mémoire: aide-mémoire.md
    - Intégrales: intégrales.md
    - Consignes: Consignes.md
    - Equations différentielles : équations_différentielles.md

       
plugins:
    - thumbnails:
        style: margin-top:5px;margin-bottom:5px;margin-right:25px       
    
                                    # JUSTIFICATIONS
theme:
    name: material
    font: false                     # RGPD ; pas de fonte Google
    language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
      - scheme: default
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - scheme: slate
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - toc.integrate
        - header.autohide

extra_javascript:
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - xtra/stylesheets/ajustements.css

markdown_extensions:
    - def_list                      # Les listes de définition.
    - markdown.extensions.attr_list
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight            # Coloration syntaxique du code
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.arithmatex:
        generic: true
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed               # Volets glissants.  === "Mon volet"
    - pymdownx.superfences          # Imbrication de blocs.
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg

extra:
    social:
        - icon: fontawesome/brands/gitlab # ou autre
          link: https://gitlab.com/zeepeen