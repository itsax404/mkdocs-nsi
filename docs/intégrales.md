<meta name=viewport content="width=device-width,initial-scale=1">  
<meta charset="utf-8"/>

<script src="https://www.geogebra.org/apps/deployggb.js"></script>

# Intégrales

En 1696, Jacques Bernoulli reprend le mot latin "integer" déjà utilisé au $\text{XIV}^{ème}$ siècle, pour désigner le calcul intégral. 
A cette époque, on partait de l'équation de la courbe pour calculer l'aire sous la courbe, c'est-à-dire du "bord" de la surface à la surface entière (intégrale).


## Intégrale et aire

### Unité d'aire

!!! note "Définition 📖"

    Dans le repère (O, I, J), le rectangle rouge a pour dimension 1 sur 1.  
    Il s'agit du rectangle "unité", qui a pour aire 1 unité d'aire. On écrit u.a.  
    L'aire du rectangle vert est égale à 8 fois l'aire du rectangle rouge.  
    Donc, l'aire du rectangle vert est égale à 8 u.a.  
    Lorsque les longueurs unitaires sont connues, il est possible de convertir les unités d'aires en unités de mesure (le $cm^2$ par exemple).  
    ![Image des rectangles](./images/rectangles.png)  
    *Source [^1]*
[^1]: Source : cours de Mme CASTELLI.

    
###  Intégrale

!!! note "Définition 📖"
    Soit $f$ une fonction continue et positive sur un intervalle $[a;\:b]$.  
    On appelle **intégrale** de $f$ sur $[a;\:b]$, l'aire exprimiée en u.a, de la surface délimitée par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites d'équations $x = a$ et $x = b$.  
    ![Image de la courbe représentative de f](./images/courbe_definition_intégrales.png)  
    *Source [^1]*    

    !!! note "Notation ✍"
        L'intégrale de la fonction $f$ sur l'intervalle $[a;\:b]$ se note :  
        $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x$   
        On lit cela : "intégrale de $a$ à $b$ de $f(x) \: \mathrm{d}x$".

    ??? tip "Point Histoire 👨‍🏫"
        Cette notation est due au mathématicien allemand *Gottfried Willhelm bon Leibniz* (1646 | 1716).  
        Ce symbole fait pense à un "S" allongé et s'explique par le fait que l'intégrale est égale à une aire calculée comme une **s**omme infinie d'autres aires.  
        Plus tard, un second mathématicien allement, *Bernhard Riemann* (1826 | 1866) établit une théorie aboutie du calcul intégral.  

    ??? tip "Remarques ❗"
        - $a$ et $b$ sont appelés les ^^bornes d'intégration^^.  
        - $x$ est la variable. Elle peut être remplacée par tout autres lettre qui n'intervient pas ailleurs.  
        Ainsi, on peut écrire : $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x = \displaystyle \int_{a}^{b} f(x) \: \mathrm{d}t$  
        "$\mathrm{d}x$" ou "$\mathrm{d}t$" nous permet de reconnaître la variable d'intégration.  

??? example "Exemple :"
    L'aire de la surface délimitée par la courbe représentative de la fonction $f$ définie par $f(x) = x^2 + 1$, l'axe des abscisses et les droites d'équations $x = -2$ et $x = 1$ est l'intégrale de la fonction $f$ sur l'intervalle $[-2;\:1]$ et se note $\displaystyle \int_{-2}^{1} x^{2}+1(x) \: \mathrm{d}x$  
    ![Image de la fonction x²](./images/exemple_intégrale_I_1.png)  
    *Source [^1]*


??? example "Déterminer une intégrale par calculs d'aire :"
    Calculer $\displaystyle \int_{-2}^{3} f(x) \: \mathrm{d}x$  
    ![Image "Déterminer une intégrale par calculs d'aire"](./images/intégrale_par_calculs_aires.png)  
    *Source [^1]*  
    Calculer $\displaystyle \int_{-2}^{3} f(x) \: \mathrm{d}x$ revient à calculer l'aire de la surface delimitée par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites d'équations $x = -2$ et $x = 3$.  
    Donc, par dénombrement et calculs d'aires de figures géométriques connues, on obtient :  
    $\displaystyle \int_{-2}^{3} f(x) \: \mathrm{d}x$ = $7$ $u.a$ $+$ $\dfrac{1}{2}$ $u.a$ $+$ $\dfrac{2 \times 1}{2}$ $u.a$ $=$ $8,5$  $u.a.$

### Encadrement de l'intégrale d'une fonction monotone et postive

Soit une fonction $f$ continue, positive et monotone sur l'intervalle $[a;\:b]$.  
On partage l'intervalle $[a; b]$ en $n$ sous-intervalles de même amplitude $\dfrac{b - a}{n}.  
Sur chaque petit intervalle, on détermine la valeur maximale et minimale de la fonction $f$.  
L'aire sous la courbe est alors encadrée par deux suites correspondantes à l'aire des rectangles rouges et verts.  
Ces deux suites $A_{k}$ et $B_{k}$ convergent vers la même limite :   
$\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x$

![Image de la courbe entouré par les rectangles](./images/théorème_des_rectangles.png)  
*Source [^1]*  
Ici, sur tout l'intervalle $[0;\:1]$, l'aire sous la courbe est comprise entre la somme des $n$ rectangles "inférieurs" et la somme des $n$ rectangles "supérieurs".  

Voici une animation intéractive : 
<div id="ggb-element"></div> 
<script>  
    var params = {"appName": "graphing", "width": 800, "height": 600, "showToolBar": true, "showAlgebraInput": true, "showMenuBar": true, "material_id":"a8FgrUjJ" };
    var ggbApplet = new GGBApplet(params, true);
    window.addEventListener("load", function() { 
        ggbApplet.inject('ggb-element');
    });
</script>  
*Source : [^2]*

Voici un algorithme permettant d'obtenir un encadrement :

=== "Pseudo-code"
    ```
    définir fonction rectangle(a, b, n)

    l ← (b / a) / n
    x ← a
    m ← 0
    p ← 0

    pour i allant de 0 à n - 1
        m ← m + l * f(x)
        x ← x + l
        p ← p + l * f(x)
    fin pour

    afficher m et p
    ```

=== "Python"
    Pour l'algorithme en Python, nous allons donner la fonction $f(x) = x^2$ :    
    ```python
    def f(x):
        """
        Cette fonction retourne l'image de `x` par la fonction mathématique `x**2`

        >>> f(2)
        4

        >>> f(5)
        25

        >>> f(254546732753678373737376373)
        64794039155572558595878655041742451726286173458635129

        """
        return x**2

    def rectangle(a, b, n):
        """
        Cette fonction renvoie l'encadrement de l'intégrale de  `a` à `b` de `f(x)` par la méthode des rectangles.

        >>> rectangle(1, 2, 10)
        (2.1850000000000014, 2.4850000000000017)

        >>> rectangle(1, 2, 50)
        (2.3034000000000017, 2.3634000000000017)

        >>> rectangle(1, 2, 1000000)
        (2.333331833196358, 2.3333348331963575)
        """
        

        pas = (b - a) / n
        x = a
        m = 0
        p = 0
        for i in range(n):
            m += (pas * f(x))
            x += pas
            p += (pas * f(x))
        return (m, p)
    ```
    En exécutant plusieurs fois le programme sur la même intervalle mais en augmentant le nombre de sous-intervalles, le calcul s'améliore car l'encadrement formé des rectangles inférieurs et supérieurs se resserre autour de la courbe.  
    ```python 
    >>> rectangle(1, 2, 10)
    (2.1850000000000014, 2.4850000000000017)

    >>> rectangle(1, 2, 50)
    (2.3034000000000017, 2.3634000000000017)

    >>> rectangle(1, 2, 1000000)
    (2.333331833196358, 2.3333348331963575)
    ```
[^2]: Source : cours de Mme CASTELLI, auteur : Johann DOLIVET
### Extension aux fonctions de signe quelconque 

!!! note "Définition 📖"
    Soit $f$ une fonction continue sur un intervalle $[a; b]$  
    On appelle **intégrale** de $f$ sur $[a; b]$, le nombre $I = \displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x$ défini par :  
    - si $f$ est positive sur $[a, b]$, alors : $I = Aire(E)$  
    - si $f$ est négative sur $[a, b]$, alors : $I = -Aire(E)$  
    où $E$ est la surface délimitée par la courbe représentative de la fonction $f$, l'axe des abscisses et les droites d'équations $x = a$ et $x = b$.  
    ![Image représentant $E$ et $f$](./images/extension_signes_quelconques.png)  
    *Source [^1]* 
??? example "Exemple : "
    $\displaystyle \int_{2}^{5} 3 - x \: \mathrm{d}x = \dfrac{1 \times 1}{2} - {2 \times 2}{2} = -1,5$  
    ![Image représentant 3-x](./images/exemple_signes.png)  
    *Source [^1]*  

!!! note "Propriétés 📖"
    Soit $f$ une fonction continue sur un intervalle $I$ et $a$, $b$ et $c$, des réels de $I$.  
    a) $\displaystyle \int_{a}^{a} f(x) \: \mathrm{d}x = 0$  
    b) $\displaystyle \int_{b}^{a} f(x) \: \mathrm{d}x = - \int_{a}^{b} f(x) \: \mathrm{d}x$  
    c) D'après la relation de Chasles : $\displaystyle \int_{a}^{c} f(x) \: \mathrm{d}x + \int_{c}^{b} f(x) \: \mathrm{d}x = \int_{a}^{b} f(x) \: \mathrm{d}x$

!!! tip "Remarque ❗"
    Si une intégrale est nulle, alors la fonction n'est pas nécessairement nulle.  
    Par exemple :  
    $\displaystyle \int_{-2}^{2} x^{3} \: \mathrm{d}x = \int_{-2}^{0} x^{3} \: \mathrm{d}x + \int_{0}^{2} x^{3} \: \mathrm{d}x$  
    La courbe représentative de la fonction cube est en symétrique par rapport à l'origine du repère donc :  
    $\displaystyle \int_{-2}^{0} x^{3} \: \mathrm{d}x = -\int_{0}^{2} x^{3} \: \mathrm{d}x$  
    ![Image de la fonction x**3](./images/remarque_intégrale_nulle.png)  
    *Source [^1]*

??? example "Exemple :"
    Reprenons le tout premier exemple :  
    $\displaystyle \int_{-2}^{3} f(x) \: \mathrm{d}x = \int_{-2}^{-1} f(x) \: \mathrm{d}x + \int_{-1}^{1} f(x) \: \mathrm{d}x + \int_{1}^{3} f(x) \: \mathrm{d}x$  
    $\displaystyle \int_{-2}^{3} f(x) \: \mathrm{d}x = \dfrac{(1 + 2) \times 1}{2} + 2 \times 2 + \dfrac{(1 + 2) \times 2}{2} = 8,5 u.a.$

## Intégrale et primitive

### Fonction définie par une intégrale

!!! note "Théorème 📖"
    Soit $f$ une fonction continue sur un intervalle $[a;\:b]$.  
    La fonction $F$ définie sur $[a; b] par $F(x) = \int_{a}^{x} f(t) \: \mathrm{d}t$ est la primitive de $f$ qui s'annule en $a$.

    ![Image de f](./images/image_théorème_intégrale.png)  
    *Source [^1]*

!!! note "Démonstration au programme de Terminale :"
    Dans le cas où $f$ est strictement croissante :   
    - $1^{\text{er}}$cas : $h > 0$ :  
    On considère deux réels $x$ et $x + h$ de l'intervalle $[a;\:b]$.  
    On veut démontrer que $\displaystyle \lim\limits_{h \rightarrow 0} \dfrac{F(x+h) - F(x)}{h} = f(x)$.  
    $\displaystyle F(x + h) - F(x) = \int_{a}^{x + h} f(x) \: \mathrm{d}x - \int_{a}^{x} f(x) \mathrm{d}x$    
    $\displaystyle F(x + h) - F(x) = \int_{a}^{x + h} f(x) \: \mathrm{d}x + \int_{x}^{a} f(x) \: \mathrm{d}x$  
    $\displaystyle F(x + h) - F(x) = \int_{x}^{x + h} f(x) \: \mathrm{d}x$  
    On a représenté ci-dessous, la courbe de loa fonction $f$ (en vert).  
    Cette différence est égale à l'aire de la surface colorée en rouge.  
    ![Image correspondante à la démonstration](./images/démonstration_f_croissante.png)  
    *Source [^1]*  

    Elle est comprise entre les aires des rectangles $ABFE$ et $ABHG$.  
    Or, $Aire(ABFE) = h \times f(x)$ et $Aire(ABHG) = h \times f(x + h)$.  
    Comme $f$ est croissante sur $[a; b]$, on a :  
    $h \times f(x) < F(x + h) - F(x) < h \times f(x + h)$  
    Puisque $h > 0$, on a :  
    $$\displaystyle f(x) < \dfrac{F(x + h) - F(x)}{h} < f(x + h) $$  
    Comme $f$ est continue sur $[a; b]$, $\displaystyle \lim\limits_{h \rightarrow 0} f(x + h) = f(x)$.  
    D'après le théomème des gendarmes, $\displaystyle \lim\limits_{h \rightarrow 0} \dfrac{F(x + h) - F(x)}{h} = f(x)$.  
    Et donc $F'(x) = f(x)$.  
    $F$ est donc une primitive de $f$.  
    Par ailleurs, $F$ s'annule en $a$, car $\displaystyle F(a) = \int_{a}^{a} f(t) \: \mathrm{d}t = 0$.

    - $2^{\text{ème}}$ cas : $h < 0$:  
    La démonstration est analogue (les encadrements sont inversées).  

    - Conséquence immédiate :  
    !!! note "Théorème 📖"
        Toute fonction continue sur un intervalle admet des primitives.  

### Calcul d'intégrales

!!! note "Propriété 📖"
    Soit $f$, une fonction continue sur un intervalle $[a;\:b]$.  
    Si $F$ est une primitive de $f$, alors $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x = F(b) - F(a)$.

!!! note "Démonstration au programme de Terminale :"  
    La fonction $G$ définie sur $[a; b]$ par $\displaystyle G(x) = \int_{a}^{x} f(t) \: \mathrm{d}t$ est une primitive de $f$ sur $[a; b]$ d’après le premier théorème du paragraphe II.  
    Si $F$ est une primitive de $f$ alors pour tout $x$ de $[a ; b]$, on a : $G(x) = F(X) + k, k ∈ \mathbb{R}$.  
    En effet, deux primitives d’une même fonction diffèrent d’une constante.  
    De plus, $\displaystyle G(a) = \int_{a}^{a} f(t) \: \mathrm{d}t = 0$ et $G(a) = F(a) + k$ donc $F(a) = -k$ et $k = -F(a)$.  
    Or $\displaystyle G(b) = \int_{a}^{b} f(t) \: \mathrm{d}t = F(b) + k = F(b) - F(a)$.    

!!! note "Définition 📖"
    Soit $f$, une fonction **continue** sur un intervalle $I$, $a$ et $b$, dexu réels de $I$ et $F$ une primitive de $f$ sur $[a; b]$.  
    On appelle **intégrale** de $f$ sur $[a; b]$ la différence $F(b) - F(a)$.  

!!! tip "Notation ✍"
    $$\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x = \left[F(x)\right]_{a}^{b} = F(b) - F(a)$$

??? example "Calculer une intégrale à partir d'une primitive :"
    Calculer les intégrales suivantes :  
    $\displaystyle A = \int_{1}^{4} \dfrac{3}{x^2} \: \mathrm{d}x$  
    $\displaystyle B = \int_{2}^{5} 3x^2 + 4x - 5 \: \mathrm{d}x$  
    $\displaystyle C = \int_{-1}^{1} e^{-2x} \: \mathrm{d}x$    

    $\displaystyle A = \int_{1}^{4} \dfrac{3}{x^2} \: \mathrm{d}x$  
    On note : $f(x) = \dfrac{3}{x^2} = 3 \times \dfrac{1}{x^2}$.  
    Une primitive de $f$ est $F$ tel que : $F(x) = 3 \times (-\dfrac{1}{x})$  
    Donc : 
    $\displaystyle A = \int_{1}^{4} \dfrac{3}{x^2} \: \mathrm{d}x = \left[-\dfrac{3}{x} \right]_{1}^{4} = F(4) - F(1) = -\dfrac{3}{4} - (-\dfrac{3}{1}) = \dfrac{9}{4}$.  

    $\displaystyle B = \int_{2}^{5} 3x^2 + 4x - 5 \: \mathrm{d}x$  
    $\displaystyle B = \left[x^3 + 2x^2 - 5x \right]_{2}^{5}$  
    $\displaystyle B = 5^3 + 2 \times 5^2 - 5 \times 5 - (2^3 + 2 \times 2^2 - 5 \times 2) = 144$  

    $\displaystyle C = \int_{-1}^{1} e^{-2x} \: \mathrm{d}x$  
    On note : $f(x) = e^{-2x} = \dfrac{1}{-2} \times (-2)e^{-2x}$  
    Une primitive de $f$ est $F$ tel que : $F(x) = \dfrac{1}{-2}e^{-2x}$  
    Donc :  
    $\displaystyle C = \int_{-1}^{1} e^{-2x} \: \mathrm{d}x = \left[\dfrac{1}{-2}e^{-2x} \right]_{-1}^{1} = F(1) - F(-1)$  
    $\displaystyle C = \dfrac{1}{-2}e^{-2 \times 1} - \dfrac{1}{-2}e^{-2 \times (-1)}$  
    $\displaystyle C = -\dfrac{1}{2}e^{-2} + \dfrac{1}{2}e^{2}$  
    $\displaystyle C = \dfrac{1}{2}(e^{2} - \dfrac{1}{e^{2}})$  

### Propriété de linéarité 
!!! note "Propriétés 📖"
    Soit $f$ et $g$, deux fonctions continues sur un intervalle $I$.  
    $a$ et $b$ deux réels de $I$.  
    a) Pour $k$ réel, $\displaystyle \int_{a}^{b} kf(x) \: \mathrm{d}x = k \int_{a}^{b} f(x) \: \mathrm{d}x$  
    b) $\displaystyle \int_{a}^{b} f(x) + g(x) \: \mathrm{d}x = \int_{a}^{b} f(x) \: \mathrm{d}x + \int_{a}^{b} g(x) \: \mathrm{d}x$

    ??? tip "Éléments de démonstration :"  
        On applique ces propriétés :  
        - $kF$ est une primitive de $kf$  
        - $F + G$ est une primitive de $f + g$  

??? example "Calculer une intégrale en appliquant la linéarité :" 
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/B9n_AArwjKw){.youtube}  
    On pose : $\displaystyle A = \int_{0}^{2\pi} cos^{2}(x) \: \mathrm{d}x$ et $\displaystyle B = \int_{0}^{2\pi} sin^{2}(x) \: \mathrm{d}x$  
    a) Calculer $A + B$ et $A - B$  
    b) En déduire $A$ et $B$  
    a) On calculer en appliquant les fomrules de linéarité :  
    $\displaystyle A + B = \int_{0}^{2\pi} cos^{2}(x) \: \mathrm{d}x + \int_{0}^{2\pi} sin^{2}(x) \: \mathrm{d}x$
    $\displaystyle A + B = \int_{0}^{2\pi} cos^2(x) + sin^2(x) \: \mathrm{d}x$  
    $\displaystyle A + B = \int_{0}^{2\pi} 1 \: \mathrm{d}x$  
    $\displaystyle A + B = \left[x \right]_{0}^{2\pi}$
    $A + B = 2\pi$  
    $\displaystyle A - B = \int_{0}^{2\pi} cos^{2}(x) \: \mathrm{d}x - \int_{0}^{2\pi} sin^{2}(x) \: \mathrm{d}x$
    $\displaystyle A - B = \int_{0}^{2\pi} cos^2(x) - sin^2(x) \: \mathrm{d}x$  
    $\displaystyle A - B = \int_{0}^{2\pi} cos(2x) \: \mathrm{d}x$  
    $\displaystyle A - B = \left[\dfrac{1}{2}sin(2x) \right]_{0}^{2\pi}$
    $A - B = \dfrac{1}{2}sin(2 \times 2\pi) - \dfrac{1}{2}sin(2 \times 0) = 0$  
    b) On a ainsi :

    \begin{cases}  
    A + B = 2\pi \\   
    A - B = 0
    \end{cases}  
    
    donc :  

    \begin{cases}
    2A = 2\pi \\   
    A = B
    \end{cases}

    soit : $A = B = \pi$  

### Inégalités

!!! note "Propriétés 📖 "  
    Soit $f$ et $g$, deux fonctions continues sur un intervalle $I$.  
    $a$ et $b$ deux réels de $I$ avec a \leq b$.  
    a) Si, pour tous $x$ de $[a; b]$, alors $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x \geq \int_{a}^{b} g(x) \: \mathrm{d}x$  
    b) Si, pour tous $x$ de $[a; b]$, $f(x) \geq g(x)$, alors $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x \geq \int_{a}^{b} g(x) \: \mathrm{d}x$  

!!! note "Démonstration :"
    a) Par définition, lorsque $f$ est positive, l'intégrale de $f$ est une aire donc est positive.  
    b) Si $f(x) \geq g(x)$ alors $f(x) - g(x) \geq 0$  
    Donc, en appliquant le a), on a : $\displaystyle \int_{a}^{b} f(x) - g(x) \: \mathrm{d}x \geq 0$.  
    Par linéarité, on a $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x - \int_{a}^{b} g(x) \: \mathrm{d}x \geq 0$ et donc $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x \geq \int_{a}^{b} g(x) \: \mathrm{d}x$.  
??? example "Encadrer une intégrale :"
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/VK0PvzWBIso){.youtube}  
    a) Démontrer que, pour tout $x$ de $[0; 1]$, on a : $0 \leq e^{x^{2}} \leq e^{x}$  
    b) En déduire que : $\displaystyle 0 \leq \int_{0}^{1} e^{x^{2}} \: \mathrm{d}x \leq e - 1$  

    a) Sur $[0; 1]$, $x^2 \leq x$.  
    Comme la fonction exponentielle est croissante et positive sur $\mathbb{R}$, on a : $0 \leq e^{x^{2}} \leq e^{x}$  
    b) On en déduit de la question précédente que :   
    $\displaystyle \int_{0}^{1} 0 \: \mathrm{d}x \leq \int_{0}^{1} e^{x^{2}} \: \mathrm{d}x \leq e^{x}$  
    D'où : $\displaystyle 0 \leq \int_{0}^{1} e^{x^{2}} \: \mathrm{d}x \leq e - 1$

## Valeur moyenne d'une fonction 

!!! note "Définition 📖"
    Soit $f$ une fonction continue sur un intervalle $[a;\:b]$ avec $a \neq b$. 
    On appelle **valeur moyenne** de $f$ sur $[a; b]$ le nombre réel :  

    $$ µ = \dfrac{1}{b - a} \int{a}{b} f(x) \: \mathrm{d}x $$

    !!! tip "Interprétation géométrique :"
         L'aire sous la courbe représentative de $f$ (en rouge ci-dessous) est égale à l'aire sous la droite d'équation $y = m$ (en bleu), entre $a$ et $b$.  
        ![Image représentant la droite](./images/interprétation_valeur_moyenne.png)  
        *Source [^1]*  

??? example "Exemple :"
    Calculons la valeur moyenne de la fonction $f$ définie par $f(x) = 3x^2 - 4x + 5$ sur l'intervalle $[1;\:10]$.  
    $\displaystyle m = \dfrac{1}{10 - 1}\int_{1}^{10} 3x^2 - 4x + 5 \: \mathrm{d}x$  
    $\displaystyle m = \dfrac{1}{9} \left[x^3 - 2x^2 + 5x \right]_{1}^{10}$  
    $\displaystyle m = \dfrac{1}{9} \left( \left( 10^3 - 2 \times 10^2 + \times 10 \right) - \left( 1^3 - 2 \times 1^2 + 5 \times 1 \right) \right) = \dfrac{1}{9} \left(850 - 4 \right) = \dfrac{846}{9} = 94$  

!!! note "Remarque ❗"
    Cette définition est la généralisation de la moyenne d'une série statistique.

??? example "Calculer une valeur moyenne d'une fonction :"
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/VK0PvzWBIso){.youtube}    
    On modélise à l'aide d'une fonction le nombre de malades lors d'une épidémie.  
    Au $x$-ème jour après le signalement des premiers cas, le nombre de malades est égale à $f(x) = 16x^2 - x^3$  
    Déterminer le nombre moyen de malades chaque jour sur une période de 16 jours.  
    $\displaystyle m = \dfrac{1}{16 - 0}\int_{0}^{16} f(x) \: \mathrm{d}x$  
    $\displaystyle m = \dfrac{1}{16}\int_{0}^{16} 16x^2 - x^3 \: \mathrm{d}x$  
    $\displaystyle m = \dfrac{1}{16} \left[ \dfrac{16}{3}x^3 -\dfrac{1}{4}x^4 \right ]_{0}^{16}$  
    $\displaystyle m = \dfrac{1}{16} \left(\dfrac{16}{3} \times 16^3 - \dfrac{1}{4} \times 16^4 \right)$  
    $\displaystyle m = \dfrac{1024}{3} \simeq 341$  

    Le nombre moyen de malades chaque jour est environ égal à $341$.  
    ![Courbe représentant la fonction de l'exercice](./images/courbe_malades_valeur_moyenne.png)  
    *Source [^1]*  

## Calculs d'aire

!!! note "Propriétés 📖"
    !!! note "Propriété 1 :"
        Soit $f$ une fonction continue sur $[a; b]$ positive sur $[a; c]$ et négative sur $[c; b]$.  
        Alors $\displaystyle \int_{a}^{b} f(x) \: \mathrm{d}x = Aire(D1) - Aire(D2)$ où $D1$ est l'aire du domaine délimité par la courbe représentative de $f$, l'axe des abscisses et les droites d'équation $x = a$ et $x = c$.  
        $D2$ est l'aire du domaine délimité par la courbe représentative de $f$, l'axe des abscisses et les droites d'équation $x = c$ et $x = b$.  
        ![Courbe de f](./images/courbe_propriété_calculs_d_aire.png)  
        *Source [^1]*  
    
    !!! note "Propriété 2 :"
        Soit $f$ et $g$ deux fonctions continues sur un intervalle $[a;\:b]$ telles que $g \leq f$.  
        Soit $D$, l'aire du domaine délimité par les courbes représentatives de $f$ et $g$, les droites d'équation $x = a$ et $x = b$.  
        Alors :  
        $\displaystyle D = \int_{a}^{b} f(x) - g(x) \: \mathrm{d} x$  
        ![Courbe de f - g](./images/propriété_2_calculs_aire.png)  
        *Source [^1]*  

??? example "Calculer l'aire délimitée par les courbes de deux fonctions continues et positives : "
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/oRSAYNwUiHQ){.youtube}  
    On considère les fonctions $f$ et $g$ définies par $f(x) = x^2 + 1$ et $g(x) = -x^2 + 2x +5$.  
    On admet que pour tout $x$ de $[-1; 2]$, on a $f(x) \leq g(x)$  
    Déterminer l'aire délimitée par les courbes représentatives de $f$ et de $g$ sur l'intervalle $[-1;\:2]$.  
    $\displaystyle A = \int_{-1}^{2} g(x) \: \mathrm{d}x - \int_{-1}^{2} f(x) \: \mathrm{d}x$  
    $\displaystyle A = \int_{-1}^{2} -x^2 + 2x + 5 \: \mathrm{d}x - \int_{-1}^{2} x^2 + 1 \: \mathrm{d}x$  
    $\displaystyle A = \int_{-1}^{2} -x^2 + 2x + 5 -x^2 - 1 \: \mathrm{d}x$  
    $\displaystyle A = \int_{-1}^{2} -2x^2 + 2x + 4 \: \mathrm{d}x = \cdots = 9$  
    ![Courbe de l'exercice](./images/calcul_aire_delimitée_2_fonctions.png)  
    *Source [^1]*  

## Intégration par parties

!!! note "Théorème 📖"
    Soit $u$ et $v$ deux fonctions dérivables $[a; b]$.  
    Alors, on a :  
    $$ \int_{a}^{b} u'(x)v(x) \: \mathrm{d}x = \left[u(x)v(x) \right]_{a}^{b} - \int_{a}^{b} u(x)v'(x) \: \mathrm{d}x $$

!!! note "Démonstration au programme de Terminale :"
    $uv$ est dérivable sur $[a; b]$ et on a : $(uv)' = u'v + uv'$.  
    Les fonctions $uv'$, $u'v^et $(uv)'$ sont continues sur $[a; b]$, donc :  
    $\displaystyle \left[u(x)v(x) \right]_{a}^{b} = \int_{a}^{b} (uv)'(x) \: \mathrm{d}x$  
    $\displaystyle \left[u(x)v(x) \right]_{a}^{b} = \int_{a}^{b} (u'v + uv')(x) \: \mathrm{d}x$  
    $\displaystyle \left[u(x)v(x) \right]_{a}^{b} = \int_{a}^{b} (u'v)(x) \: \mathrm{d}x + \int_{a}^{b} (uv')(x) \: \mathrm{d}x$  
    $\displaystyle \left[u(x)v(x) \right]_{a}^{b} = \int_{a}^{b} u'(x)v(x) \: \mathrm{d}x + \int_{a}^{b} u(x)v'(x) \: \mathrm{d}x$  
    D'où :  
    $\displaystyle \int_{a}^{b} u'(x)v(x) \: \mathrm{d}x = \left[u(x)v(x) \right]_{a}^{b} - \int_{a}^{b} u(x)v'(x) \: \mathrm{d}x$  

??? example "Calculer une intégrale en intégrant par parties :"
    Cet exercice a été fait en vidéo par Yves Monka :  
        - [Partie 1](https://youtu.be/uNIpYeaNfsg)  
        - [Partie 2](https://youtu.be/vNQeSEb2mj8)       
        - [Partie 3](https://youtu.be/xbb3vnzF3EA)   

    Caluler les intégrales suivantes :  
    $\displaystyle A = \int_{0}^{\frac{\pi}{2}} xsin(x) \: \mathrm{d}x$  
    $\displaystyle B = \int_{0}^{\frac{\pi}{2}} x^{2}cos(x) \: \mathrm{d}x$  
    $\displaystyle C = \int_{0}^{\frac{\pi}{2}} \ln(x) \: \mathrm{d}x$      
    $\displaystyle A = \int_{0}^{\frac{\pi}{2}} xsin(x) \: \mathrm{d}x$  
    On pose = $v(x) = x \rightarrow v'(x) = 1$ et $u'(x) = sin(x) \rightarrow u(x) = -cos(x)$  
    (*Ce choix n'est pas anodin ! L'idée est ici de ne plus laisser de facteur $x$ dans l'expression qu'il restera à intégrer. Il faudrait donc dériver $x$*).  
    Ainsi, en intégrant par parties, on a :  
    $\displaystyle A = \int_{0}^{\frac{\pi}{2}} u'(x)v(x) \: \mathrm{d}x = \left[u(x)v(x) \right]_{0}^{\frac{\pi}{2}} - \int_{0}^{\frac{\pi}{2}} u(x)v'(x) \: \mathrm{d}x$  
    $\displaystyle A = \left[-cos(x) \times x \right]_{0}^{\frac{\pi}{2}} - \int_{0}^{\frac{\pi}{2}} -cos(x) \times 1 \: \mathrm{d}x$  
    $\displaystyle A = \left[-xcos(x) \right]_{0}^{\frac{\pi}{2}} - \int_{0}^{\frac{\pi}{2}} cos(x) \: \mathrm{d}x$  
    $\displaystyle A = -\dfrac{\pi}{2}cos(\dfrac{\pi}{2}) + 0 \times cos(0) + \left[sin(x) \right]_{0}^{\frac{\pi}{2}}$  
    $\displaystyle A = sin \left( \dfrac{\pi}{2} \right) - sin(0) = 1$  

    $\displaystyle B = \int_{0}^{\frac{\pi}{2}} x^{2}cos(x) \: \mathrm{d}x$  
    On pose : $v(x) = x^2 \rightarrow v'(x) = 2x$  et $u'(x) = cos(x) \rightarrow u(x) = sin(x)$  
    Ainsi, en intégrant par parties, on a :  
    $\displaystyle B = \int_{0}^{\frac{\pi}{2}} u'(x)v(x) \: \mathrm{d}x = \left[u(x)v(x) \right]_{0}^{\frac{\pi}{2}} - \int_{0}^{\frac{\pi}{2}} u(x)v'(x) \: \mathrm{d}x$  
    $\displaystyle B = \left[sin(x) \times x^2 \right]_{0}^{\dfrac{\pi}{2}} - \int_{0}^{\frac{0}{\frac{\pi}{2}}} sin(x) \times 2x \: \mathrm{d}x$  
    $\displaystyle B = \left[x^{2}sin(x) \right]_{0}^{\frac{\pi}{2}} - 2\int_{0}^{\frac{\pi}{2}} xsin(x) \: \mathrm{d}x$  
    Or, dans le terme de droite, on reconnait l'intégrale A de la question précédente qui a été calculée par parties. Il s'agit ici d'une **double intégration par parties**.  
    On a donc : 
    $B = \left( \dfrac{\pi}{2} \right) sin(\dfrac{\pi}{2}) - 0^2sin(0) - 2 \times 1$  
    $B = \dfrac{\pi^2}{4} - 2$  
    $\displaystyle C = \int_{1}^{e^2} 1 \times \ln(x) \: \mathrm{d}x$  
    On pose : $v(x) = \ln(x) \rightarrow v'(x) = \dfrac{1}{x}$ et $u'(x) = 1 \rightarrow u(x) = x$  
    Ainsi, en intégrant par parties, on a :  
    $\displaystyle C = \int_{1}^{e^2} u'(x)v(x) \: \mathrm{d}x = \left[u(x)v(x) \right]_{1}^{e^2} - \int_{1}^{e^2} u(x)v'(x) \: \mathrm{d}x$